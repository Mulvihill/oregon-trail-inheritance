function Traveler (name, food, isHealthy) {
    this.name = name;
    this.food = food = 1;
    this.isHealthy = true;
    
}
Traveler.prototype = {
    constructor: Traveler(this.name),

    hunt: function () {
        this.food += 2;
        return this.food;
    },

    eat: function () {
        if (this.food <= 0) {
            this.isHealthy = false;
        }
        else {
            this.food -= 1;
            if (this.food <= 0) {
                this.isHealthy = false;
            }
            return this.food;
        }
    },

    
    
    
}

function Doctor (name, food, isHealthy) {
    Traveler.call(this, name, food, isHealthy);
}

Doctor.prototype = Object.create(Traveler.prototype);

Doctor.prototype = {
    constructor: Doctor(this.name),
    
    heal: function (tr) {
        if (tr.isHealthy==false) {
            tr.isHealthy = true;
        }
    },
    hunt: function () {
        this.food += 2;
        return this.food;
    },

    eat: function () {
        if (this.food <= 0) {
            this.isHealthy = false;
        }
        else {
            this.food -= 1;
            if (this.food <= 0) {
                this.isHealthy = false;
            }
            return this.food;
        }
    },
 
}

function Hunter (name, food, isHealthy) {
    Traveler.call(this, name, isHealthy);
    this.food = 2;
}

Hunter.prototype = Object.create(Traveler.prototype);
Hunter.prototype = {
    constructor: Hunter(this.name),

    hunt: function () {
        this.food += 5;
        return this.food;
    },
    eat: function () {
        if (this.food <= 0) {
            this.isHealthy = false;
        }
        else {
            this.food -= 2;
            if (this.food <= 0) {
                this.food = 0;
                this.isHealthy = false;
            }
            return this.food;
        }
    },

    giveFood: function (nam, numberOfFoodUnits) {
        if (this.food < numberOfFoodUnits) {
            console.log('Not enough food to give')
        }
        else {
            this.food = this.food - numberOfFoodUnits;
            nam.food += numberOfFoodUnits;
        }
    }
}

function Wagon (capacity, passengers) {
    this.capacity = capacity;
    this.passengers = passengers = [];
}

Wagon.prototype = {
    constructor: Wagon(this.capacity),

    getAvailableSeatCount: function() {
        x = this.capacity - this.passengers.length;
        return x;
    },

    join: function(person) {
        if (this.getAvailableSeatCount() > 0) {
            this.passengers.push(person);
        }
        else {return "There is no room!"}
    },

    shouldQuarantine: function () {
        y = 0;
        for (tr in this.passengers) {
            if(this.passengers[tr].isHealthy === false) {
                return true;
            }
            else if (this.passengers[tr] == this.passengers.length - 1 && this.passengers[tr].isHealthy() === true ) {
                return false;
            }
        }
    },

    totalFood: function () {
        f = 0;
        for (tr in this.passengers) {
            f += this.passengers[tr].food;
        }
        return f;
    }
}


// Create a wagon that can hold 4 people
let wagon = new Wagon(4);
// Create five travelers
let henrietta = new Traveler('Henrietta');
let juan = new Traveler('Juan');
let drsmith = new Doctor('Dr. Smith');
let sarahunter = new Hunter('Sara');
let maude = new Traveler('Maude');
console.log(`#1: There should be 4 available seats. Actual: ${wagon.getAvailableSeatCount()}`);
wagon.join(henrietta);
console.log(`#2: There should be 3 available seats. Actual: ${wagon.getAvailableSeatCount()}`);
wagon.join(juan);
wagon.join(drsmith);
wagon.join(sarahunter);
wagon.join(maude); // There isn't room for her!
console.log(`#3: There should be 0 available seats. Actual: ${wagon.getAvailableSeatCount()}`);
console.log(`#4: There should be 5 total food. Actual: ${wagon.totalFood()}`);
sarahunter.hunt(); // gets 5 more food
drsmith.hunt();
console.log(`#5: There should be 12 total food. Actual: ${wagon.totalFood()}`);
henrietta.eat();
sarahunter.eat();
drsmith.eat();
juan.eat();
juan.eat(); // juan is now hungry (sick)
console.log(`#6: Quarantine should be true. Actual: ${wagon.shouldQuarantine()}`);
console.log(`#7: There should be 7 total food. Actual: ${wagon.totalFood()}`);
drsmith.heal(juan);
console.log(`#8: Quarantine should be false. Actual: ${wagon.shouldQuarantine()}`);
sarahunter.giveFood(juan, 4);
sarahunter.eat(); // She only has 1, so she eats it and is now sick
console.log(`#9: Quarantine should be true. Actual: ${wagon.shouldQuarantine()}`);
console.log(`#10: There should be 6 total food. Actual: ${wagon.totalFood()}`);